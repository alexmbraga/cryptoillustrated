/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package cryptohistory;

/**
 *
 * @author Alexandre
 */
public interface Cipher {
    public abstract String encrypt(String plaintext);
    //public abstract String encrypt(String s, String ek);
    public abstract String decrypt(String ciphertext);
    //public abstract String decrypt(String s, String dk);

    public abstract void setKey(String secretKey);
}