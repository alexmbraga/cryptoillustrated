package cryptohistory;

import java.util.HashMap;

/**
 *
 * @author Alexandre
 */
public class AfimCipher implements Cipher {
    static String alphabet = "abcdefghijklmnopqrstuvwxyz";
    static HashMap<Integer, Integer> relativePrimes
     = new HashMap<Integer, Integer>();

    // os primos relativos a e p obedecem a relação
    // a*p == 1 mod 26
    // (1,1); (3,9); (5,21); (7,15); (11,19); (17,23); (25,25)
    static { 
        relativePrimes.put( 1, 1);
        relativePrimes.put( 3, 9);
        relativePrimes.put( 5,21);
        relativePrimes.put( 7,15);
        relativePrimes.put( 9, 3);
        relativePrimes.put(11,19);
        relativePrimes.put(15, 7);
        relativePrimes.put(17,23);
        relativePrimes.put(19,11);
        relativePrimes.put(21, 5);
        relativePrimes.put(23,17);
        relativePrimes.put(25,25);
    }

    private int a = 1; // ceasar cipher is the default
    private int b = 3;

    public String encrypt(String plaintext){
        char[] ciphertext = new char[plaintext.length()];
        for (int i = 0; i < plaintext.length(); i++){
            char c = plaintext.toLowerCase().charAt(i);
            int shift = (a*(alphabet.indexOf(c)) + b) % alphabet.length();
            ciphertext[i] = alphabet.charAt(shift);
        }
        return new String(ciphertext);
    }

    public String decrypt(String ciphertext){
        char[] plaintext = new char[ciphertext.length()];
        for (int i = 0; i < ciphertext.length(); i++){
            char c = ciphertext.toLowerCase().charAt(i);
            int index = alphabet.indexOf(c);
            int p = relativePrimes.get(a);
            int shift = (p*alphabet.length() + p*(index - b)) % alphabet.length();
            plaintext[i] = alphabet.charAt(shift);
        }
        return new String(plaintext);
    }

    
    public void setKey(String secretKey){
            String[] key = secretKey.split(",");
            if (key !=null && key.length == 2) {
                a = Integer.parseInt(key[0]);
                b = Integer.parseInt(key[1]);
            }
            // verifica valores válidos de a
            // 1, 3, 5, 7, 9, 11, 15, 17, 19, 21, 23, 25
            if (!relativePrimes.containsKey(a)) a = 1;
            b = b % alphabet.length();
    }

    public static void main(String args[]) {
        Cipher c = new AfimCipher();
        String msg = "AlexandreMeloBraga";
        int n = 1;
        for (Object o : relativePrimes.values()) {
            for (int i = 0; i < alphabet.length(); i++) {
                System.out.println("chave "+ n +": "+o+","+i); n++;
                c.setKey(o+","+i);
                String criptograma = c.encrypt(msg);
                System.out.println("criptograma: " + criptograma);
                System.out.println("texto claro: " + c.decrypt(criptograma));
            }
        }
    }
}
