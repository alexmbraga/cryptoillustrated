/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cryptohistory;

import java.io.UnsupportedEncodingException;
/**
 *
 * @author Alexandre
 */
public class XorCipher implements Cipher {

    private String secretKey = "\\]^_`a|}~!\"%&'(";

    public XorCipher() { secretKey = null;}
    public void setKey(String secretKey) { this.secretKey = secretKey;}

    private static byte[] crypt(byte M[], byte K[]) {
        byte C[] = null;
            C = new byte[M.length];
            for (int i = 0; i < M.length; i++) {
                C[i] = (byte) (M[i] ^ K[i % K.length]);
            }
        return C;
    }

    public String encrypt(String cleartext) {
        String encrypted = null;
        try {
            byte clear[] = cleartext.getBytes("US-ASCII");
            byte key[] = secretKey.getBytes("US-ASCII");
            byte cipher[] = crypt(clear, key);
            encrypted = new String(cipher, "US-ASCII");
        } catch (UnsupportedEncodingException e) {
            System.out.println(e);
            encrypted = null;
        }
        return encrypted;
    }

    public String decrypt(String ciphertext) {
        String decrypted = null;
        try {
            byte clear[] = ciphertext.getBytes("US-ASCII");
            byte key[] = secretKey.getBytes("US-ASCII");
            byte cipher[] = crypt(clear, key);
            decrypted = new String(cipher, "US-ASCII");
        } catch (UnsupportedEncodingException e) {
            System.out.println(e);
            decrypted = null;
        }
        return decrypted;
    }

    public static void main(String args[]) {
        Cipher c = new XorCipher();
        String key = "acdefghijklmnloqrs";
        String msg = "alexandremelobraga";
        c.setKey(key);
        String criptograma = c.encrypt(msg);
        System.out.println("texto claro " + hexString(msg.getBytes()));
        System.out.println("XOR         " + hexString(key.getBytes()));
        System.out.println("=");
        System.out.println("criptograma " + hexString(criptograma.getBytes()));
        System.out.println("XOR         " + hexString(key.getBytes()));
        System.out.println("=");
        System.out.println("decifrado   " + hexString(c.decrypt(criptograma).getBytes()));

        System.out.println("\nEm Binário");
        key = "abcd";
        msg = "alex";
        c.setKey(key);
        criptograma = c.encrypt(msg);
        System.out.println("texto claro " + binString(msg.getBytes()));
        System.out.println("XOR chave   " + binString(key.getBytes()));
        System.out.println("=");
        System.out.println("criptograma " + binString(criptograma.getBytes()));
        System.out.println("XOR chave   " + binString(key.getBytes()));
        System.out.println("=");
        System.out.println("decifrado   " + binString(c.decrypt(criptograma).getBytes()));
    }
    
    private static String hexString(byte[] b){
        String str = "";
        for(int i = 0; i < b.length; i++) {
            str += " ";
            str += Integer.toHexString((byte)(b[i] >> 4) & 0x0000000f );
            str += Integer.toHexString((byte) b[i] & 0x0000000f );
        }
        return str;
    }

    private static String binString(byte[] b){
        String str = "";
        for(int i = 0; i < b.length; i++) {
            str += " ";
            for (int j = 7; j >= 0; j--) {
                str += Integer.toBinaryString((byte)(b[i] >> j) & 0x00000001 );
            }
        }
        return str;
    }

}

