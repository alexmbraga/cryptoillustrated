/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cryptohistory;

/**
 *
 * @author Alexandre
 */
public class VigenereCipher implements Cipher {

    private static final String DEFAULT_KEY = "ambraga";
    private static final String alphabet = "abcdefghijklmnopqrstuvwxyz";
    private String secretKey;

    public VigenereCipher() { secretKey = DEFAULT_KEY;}

    public void    setKey(String secretKey) { this.secretKey = secretKey;}

    public String encrypt(String cleartext) {
        int perms[] = permutations(secretKey, true);
        return transform(cleartext.toLowerCase(), perms);
    }

    public String decrypt(String ciphertext) {
        int perms[] = permutations(secretKey, false);
        return transform(ciphertext, perms);
    }

    private String transform(String cleartext, int perms[]) {
        StringBuffer crypt = new StringBuffer();

        MatrixArray ma = new MatrixArray(cleartext, secretKey.length());
        int lines = ma.lines();
        for (int i = 0; i < lines; i++) {
            for (int j = 0; j < ma.cols; j++) {
                char c = ma.getChar(i, j);
                if (c != 0) {
                    int shift = (alphabet.indexOf(c) + perms[j]) % alphabet.length();
                    crypt.append(alphabet.charAt(shift));
                }
            }
        }
        return crypt.toString();
    }

    private static int[] permutations(String key, boolean crypt) {
        int perms[] = new int[key.length()];
        for (int i = 0; i < key.length(); i++) {
            char c = key.charAt(i);
            if (crypt) {
                perms[i] = alphabet.indexOf(c);
            } else {
                perms[i] = alphabet.length() - alphabet.indexOf(c);
            }
        }
        return perms;
    }

    public static void main(String args[]) {
        Cipher c = new VigenereCipher();
        c.setKey("alexmbraga");
        String msg = "AlexandreMeloBraga";
        String criptograma = c.encrypt(msg);
        System.out.println("criptograma: " + criptograma);
        System.out.println("texto claro: " + c.decrypt(criptograma));
    }

}
