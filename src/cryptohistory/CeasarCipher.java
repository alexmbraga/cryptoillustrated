package cryptohistory;

/**
 *
 * @author Alexandre
 */
public class CeasarCipher implements Cipher {
    static String alphabet = "abcdefghijklmnopqrstuvwxyz";
    
    private int key = 3; // a chave da cifra de cesar

    public String encrypt(String plaintext){
        char[] ciphertext = new char[plaintext.length()];
        for (int i = 0; i < plaintext.length(); i++){
            char c = plaintext.toLowerCase().charAt(i);
            int shift = (alphabet.indexOf(c) + key) % alphabet.length();
            ciphertext[i] = alphabet.charAt(shift);
        }
        return new String(ciphertext);
    }

    public String decrypt(String ciphertext){
        char[] plaintext = new char[ciphertext.length()];
        for (int i = 0; i < ciphertext.length(); i++){
            char c = ciphertext.toLowerCase().charAt(i);
            int index = alphabet.indexOf(c);
            int shift = (alphabet.length() + index - key) % alphabet.length();
            plaintext[i] = alphabet.charAt(shift);
        }
        return new String(plaintext);
    }

    public void setKey(String secretKey){
            key = Integer.parseInt(secretKey);
            key = key % alphabet.length();
    }

    public static void main(String args[]) {
        Cipher c = new CeasarCipher();
        for (int i = 0; i< alphabet.length(); i++){
            c.setKey(""+i);
            String msg = "AlexandreMeloBraga";
            String criptograma = c.encrypt(msg);
            System.out.println("chave "+i);
            System.out.println("criptograma: " + criptograma);
            System.out.println("texto claro: " + c.decrypt(criptograma));
        }
    }
}
