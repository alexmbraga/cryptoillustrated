package various;



import java.security.spec.*;
import javax.crypto.*;
import javax.crypto.spec.*;

class JceSunAesOperationModeTest {

    public static void main(String[] a) {

        try {
            //byte[] theKey = hexToBytes("0123456789ABCDEF0123456789ABCDEF");// 128 bits
            byte[] theKey = hexToBytes("01000100010001000100010001000100");// 128 bits
            byte[] theIVp = null;
            byte[] theMsg = ( // 128 bytes
                    "Alexandre Melo BragaAlexandre Melo BragaAlexandre Melo Braga,123")
                    .getBytes();

            String algorithm = "AES/ECB/NoPadding";
            SecretKeySpec ks  = new SecretKeySpec(theKey, "AES");
            Cipher cf = Cipher.getInstance(algorithm);
            cf.init(Cipher.ENCRYPT_MODE, ks);
            byte[] theCph = cf.doFinal(theMsg);
            // modificando 1 bit de um byte.
            theCph[1] = (byte) (theCph[1] + (byte) 0x01);
            cf.init(Cipher.DECRYPT_MODE, ks);
            byte[] theClear = cf.doFinal(theCph);
            System.out.println("\nTeste 1: " + algorithm);
            System.out.println("Bloco (bytes) : "+ cf.getBlockSize());
            System.out.println("Key : " + bytesToHex(theKey));
            System.out.println("IV  : " + bytesToHex(theIVp));
            System.out.println("Message    : " + new String(theMsg));
            System.out.println("Msg (bytes): " + bytesToHex(theMsg));
            System.out.println("Cipher     : " + bytesToHex(theCph));
            System.out.println("Clr (bytes): " + bytesToHex(theClear));
            System.out.println("Clear text : " + new String(theClear));

            algorithm = "AES/CBC/NoPadding";
            theIVp = hexToBytes("1234567890ABCDEF1234567890ABCDEF");
            ks  = new SecretKeySpec(theKey, "AES");
            cf = Cipher.getInstance(algorithm);
            AlgorithmParameterSpec aps = new IvParameterSpec(theIVp);
            cf.init(Cipher.ENCRYPT_MODE, ks, aps);
            theCph = cf.doFinal(theMsg);
            // modificando um byte.
            theCph[1] = (byte) (theCph[1] + (byte) 0x01);
            cf.init(Cipher.DECRYPT_MODE, ks, aps);
            theClear = cf.doFinal(theCph);
            System.out.println("\nTeste 2: " + algorithm);
            System.out.println("Bloco (bytes) : "+ cf.getBlockSize());
            System.out.println("Key : " + bytesToHex(theKey));
            System.out.println("IV  : " + bytesToHex(theIVp));
            System.out.println("Message    : " + new String(theMsg));
            System.out.println("Msg (bytes): " + bytesToHex(theMsg));
            System.out.println("Cipher     : " + bytesToHex(theCph));
            System.out.println("Clr (bytes): " + bytesToHex(theClear));
            System.out.println("Clear text : " + new String(theClear));

            algorithm = "AES/CFB/NoPadding";
            ks  = new SecretKeySpec(theKey, "AES");
            cf = Cipher.getInstance(algorithm);
            aps = new IvParameterSpec(theIVp);
            cf.init(Cipher.ENCRYPT_MODE, ks, aps);
            theCph = cf.doFinal(theMsg);
            // modificando um byte.
            theCph[1] = (byte) (theCph[1] + (byte) 0x01);
            cf.init(Cipher.DECRYPT_MODE, ks, aps);
            theClear = cf.doFinal(theCph);
            System.out.println("\nTeste 3: " + algorithm);
            System.out.println("Bloco (bytes) : "+ cf.getBlockSize());
            System.out.println("Key : " + bytesToHex(theKey));
            System.out.println("IV  : " + bytesToHex(theIVp));
            System.out.println("Message    : " + new String(theMsg));
            System.out.println("Msg (bytes): " + bytesToHex(theMsg));
            System.out.println("Cipher     : " + bytesToHex(theCph));
            System.out.println("Clr (bytes): " + bytesToHex(theClear));
            System.out.println("Clear text : " + new String(theClear));

            algorithm = "AES/OFB/NoPadding";
            ks  = new SecretKeySpec(theKey, "AES");
            cf = Cipher.getInstance(algorithm);
            aps = new IvParameterSpec(theIVp);
            cf.init(Cipher.ENCRYPT_MODE, ks, aps);
            theCph = cf.doFinal(theMsg);
            // modificando um byte.
            theCph[1] = (byte) (theCph[1] + (byte) 0x01);
            cf.init(Cipher.DECRYPT_MODE, ks, aps);
            theClear = cf.doFinal(theCph);
            System.out.println("\nTeste 4: " + algorithm);
            System.out.println("Bloco (bytes) : "+ cf.getBlockSize());
            System.out.println("Key : " + bytesToHex(theKey));
            System.out.println("IV  : " + bytesToHex(theIVp));
            System.out.println("Message    : " + new String(theMsg));
            System.out.println("Msg (bytes): " + bytesToHex(theMsg));
            System.out.println("Cipher     : " + bytesToHex(theCph));
            System.out.println("Clr (bytes): " + bytesToHex(theClear));
            System.out.println("Clear text : " + new String(theClear));

        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }

    public static byte[] hexToBytes(String str) {
        if (str == null) {
            return null;
        } else if (str.length() < 2) {
            return null;
        } else {
            int len = str.length() / 2;
            byte[] buffer = new byte[len];
            for (int i = 0; i < len; i++) {
                buffer[i] = (byte) Integer.parseInt(
                        str.substring(i * 2, i * 2 + 2), 16);
            }
            return buffer;
        }
    }

    public static String bytesToHex(byte[] data) {
        if (data == null) {
            return null;
        } else {
            int len = data.length;
            String str = "";
            for (int i = 0; i < len; i++) {
                if ((data[i] & 0xFF) < 16) {
                    str = str + "0" + java.lang.Integer.toHexString(data[i] & 0xFF);
                } else {
                    str = str + java.lang.Integer.toHexString(data[i] & 0xFF);
                }
            }
            return str.toUpperCase();
        }
    }
}
