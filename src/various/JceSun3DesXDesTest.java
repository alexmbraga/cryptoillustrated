package various;

import java.security.spec.*;
import javax.crypto.*;
import javax.crypto.spec.*;

class JceSun3DesXDesTest {

    public static void main(String[] a) {

        try {
            String engine = "DESede";
            String k1 = "0123456789ABCDEF";
            String k2 = "1123456789ABCDEF";
            String k3 = "2123456789ABCDEF";
            //byte[] theKey = hexToBytes(k1+k1+k1);// k1==k2==k3 (56 bits)
            //byte[] theKey = hexToBytes(k1+k2+k1);// k1==k3!=k2 (112 bits)
            byte[] theKey = hexToBytes(k1+k2+k3);// k1!=k2!=k3 (168 bits)
            byte[] theIVp = null;
            byte[] theMsg = "Alexandre Melo Braga,123".getBytes();
            String algorithm = engine+"/ECB/NoPadding";
            KeySpec ks = new DESedeKeySpec(theKey);
            SecretKeyFactory kf = SecretKeyFactory.getInstance(engine);
            SecretKey ky = kf.generateSecret(ks);
            Cipher cf = Cipher.getInstance(algorithm);
            cf.init(Cipher.ENCRYPT_MODE, ky);
            byte[] theCph = cf.doFinal(theMsg);
            cf.init(Cipher.DECRYPT_MODE, ky);
            // modificando um byte.
            //theCph[1] = (byte) (theCph[1] & (byte) 0x01);
            byte[] theClear = cf.doFinal(theCph);
            System.out.println("\nTeste 3DES EDE: " + algorithm);
            System.out.println("Bloco : "+ cf.getBlockSize());
            System.out.println("Key : " + bytesToHex(ky.getEncoded()));
            System.out.println("IV  : " + bytesToHex(theIVp));
            System.out.println("Message    : " + new String(theMsg));
            System.out.println("Msg (bytes): " + bytesToHex(theMsg));
            System.out.println("Cipher     : " + bytesToHex(theCph));
            System.out.println("Clr (bytes): " + bytesToHex(theClear));
            System.out.println("Clear text : " + new String(theClear));

            engine = "DES";
            theKey = hexToBytes(k1);
            theIVp = null;
            theMsg = "Alexandre Melo Braga,123".getBytes();
            algorithm = engine+"/ECB/NoPadding";
            ks = new DESKeySpec(theKey);
            kf = SecretKeyFactory.getInstance(engine);
            ky = kf.generateSecret(ks);
            cf = Cipher.getInstance(algorithm);
            cf.init(Cipher.ENCRYPT_MODE, ky);
            theCph = cf.doFinal(theMsg);
            cf.init(Cipher.DECRYPT_MODE, ky);
            // modificando um byte.
            //theCph[1] = (byte) (theCph[1] & (byte) 0x01);
            theClear = cf.doFinal(theCph);
            System.out.println("\nTeste DES: " + algorithm);
            System.out.println("Bloco : "+ cf.getBlockSize());
            System.out.println("Key : " + bytesToHex(theKey));
            System.out.println("IV  : " + bytesToHex(theIVp));
            System.out.println("Message    : " + new String(theMsg));
            System.out.println("Msg (bytes): " + bytesToHex(theMsg));
            System.out.println("Cipher     : " + bytesToHex(theCph));
            System.out.println("Clr (bytes): " + bytesToHex(theClear));
            System.out.println("Clear text : " + new String(theClear));
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }

    public static byte[] hexToBytes(String str) {
        if (str == null) {
            return null;
        } else if (str.length() < 2) {
            return null;
        } else {
            int len = str.length() / 2;
            byte[] buffer = new byte[len];
            for (int i = 0; i < len; i++) {
                buffer[i] = (byte) Integer.parseInt(
                        str.substring(i * 2, i * 2 + 2), 16);
            }
            return buffer;
        }
    }

    public static String bytesToHex(byte[] data) {
        if (data == null) {
            return null;
        } else {
            int len = data.length;
            String str = "";
            for (int i = 0; i < len; i++) {
                if ((data[i] & 0xFF) < 16) {
                    str = str + "0" + java.lang.Integer.toHexString(data[i] & 0xFF);
                } else {
                    str = str + java.lang.Integer.toHexString(data[i] & 0xFF);
                }
            }
            return str.toUpperCase();
        }
    }
}