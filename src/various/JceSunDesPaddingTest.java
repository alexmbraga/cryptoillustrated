package various;

import java.security.spec.*;
import javax.crypto.*;
import javax.crypto.spec.*;

class JceSunDesPaddingTest {

    public static void main(String[] a) {
        String algorithm;
        try {
            byte[] theKey = null;
            byte[] theMsg = null;
            theKey = hexToBytes("133457799BBCDFF1");

            KeySpec ks = new DESKeySpec(theKey);
            SecretKeyFactory kf = SecretKeyFactory.getInstance("DES");
            SecretKey ky = kf.generateSecret(ks);

            theMsg = hexToBytes("0123456789ABCDEF");
            algorithm = "DES/ECB/NoPadding";
            Cipher cf = Cipher.getInstance(algorithm);
            cf.init(Cipher.ENCRYPT_MODE, ky);
            byte[] theCph = cf.doFinal(theMsg);
            System.out.println("");
            System.out.println("Teste 1 : "+algorithm);
            System.out.println("Key     : " + bytesToHex(theKey));
            System.out.println("Message : " + bytesToHex(theMsg));
            System.out.println("Cipher  : " + bytesToHex(theCph));

            algorithm = "DES/ECB/PKCS5Padding";
            cf = Cipher.getInstance(algorithm);
            cf.init(Cipher.ENCRYPT_MODE, ky);
            theCph = cf.doFinal(theMsg);
            System.out.println("");
            System.out.println("Teste 2 : "+algorithm);
            System.out.println("Key     : " + bytesToHex(theKey));
            System.out.println("Message : " + bytesToHex(theMsg));
            System.out.println("Cipher  : " + bytesToHex(theCph));

            algorithm = "DES";
            cf = Cipher.getInstance(algorithm);
            cf.init(Cipher.ENCRYPT_MODE, ky);
            theCph = cf.doFinal(theMsg);
            System.out.println("");
            System.out.println("Teste 3 : "+algorithm);
            System.out.println("Key     : " + bytesToHex(theKey));
            System.out.println("Message : " + bytesToHex(theMsg));
            System.out.println("Cipher  : " + bytesToHex(theCph));

            theMsg = hexToBytes("0808080808080808"); // PKCS5Padding
            algorithm = "DES/ECB/PKCS5Padding";
            cf = Cipher.getInstance(algorithm);
            cf.init(Cipher.ENCRYPT_MODE, ky);
            theCph = cf.doFinal(theMsg);
            System.out.println("");
            System.out.println("Teste 4 : "+algorithm);
            System.out.println("Key     : " + bytesToHex(theKey));
            System.out.println("Message : " + bytesToHex(theMsg));
            System.out.println("Cipher  : " + bytesToHex(theCph));

            theMsg = hexToBytes("060606060606"); // PKCS5Padding
            algorithm = "DES/ECB/PKCS5Padding";
            cf = Cipher.getInstance(algorithm);
            cf.init(Cipher.ENCRYPT_MODE, ky);
            theCph = cf.doFinal(theMsg);
            System.out.println("");
            System.out.println("Teste 5 : "+algorithm);
            System.out.println("Key     : " + bytesToHex(theKey));
            System.out.println("Message : " + bytesToHex(theMsg));
            System.out.println("Cipher  : " + bytesToHex(theCph));


        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }

    public static byte[] hexToBytes(String str) {
        if (str == null) {
            return null;
        } else if (str.length() < 2) {
            return null;
        } else {
            int len = str.length() / 2;
            byte[] buffer = new byte[len];
            for (int i = 0; i < len; i++) {
                buffer[i] = (byte) Integer.parseInt(
                        str.substring(i * 2, i * 2 + 2), 16);
            }
            return buffer;
        }
    }

    public static String bytesToHex(byte[] data) {
        if (data == null) {
            return null;
        } else {
            int len = data.length;
            String str = "";
            for (int i = 0; i < len; i++) {
                if ((data[i] & 0xFF) < 16) {
                    str = str + "0" + java.lang.Integer.toHexString(data[i] & 0xFF);
                } else {
                    str = str + java.lang.Integer.toHexString(data[i] & 0xFF);
                }
            }
            return str.toUpperCase();
        }
    }
}

