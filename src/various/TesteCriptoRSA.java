package various;

import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;

public class TesteCriptoRSA {

    public static void main(String args[]) {
               byte[] mensagem = (
                "1234567890123456789012345678901234567890" //  40 bytes
                + "1234567890123" // 53 bytes - 512
                //+ "456789012345678901234567890" //  80 bytes
                //+ "1234567890123456789012345678901234567" // 117 bytes - 1024
                //+ "890" // 120 bytes
                //+ "1234567890123456789012345678901234567890" //160 bytes
                //+ "1234567890123456789012345678901234567890" //200 bytes
                //+ "1234567890123456789012345678901234567890" //240 bytes
                //+ "12345" // 245 - 2048
                //+"67890123456789012345678901234567890" //280 bytes
                //+ "1234567890123456789012345678901234567890" //320 bytes
                ).getBytes(); // usa PKCS1 PADDING
        try {
            KeyPairGenerator gerador = KeyPairGenerator.getInstance("RSA");
            gerador.initialize(512);
            KeyPair chaves = gerador.generateKeyPair();
            // Cria a implementacao de RSA
            Cipher cifra = Cipher.getInstance("RSA");
            // Criptografando a mensagem com a chave Publica
            // inicializa o algoritmo para criptografia
            cifra.init(Cipher.ENCRYPT_MODE, chaves.getPublic());
            // criptografa o texto inteiro
            byte[] mensagemCifrada = cifra.doFinal(mensagem);
            System.out.println("Cifrado com " + cifra.getAlgorithm());
            System.out.println("Criptograma:" + hexString(mensagemCifrada));
            // Descriptografando a mensagem com a chave Privada
            cifra.init(Cipher.DECRYPT_MODE, chaves.getPrivate());
            byte[] mensagemOriginal = cifra.doFinal(mensagemCifrada);
            System.out.println("A mensagem:" + new String(mensagemOriginal));
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private static String hexString(byte[] b) {
        String str = "";
        for (int i = 0; i < b.length; i++) {
            //str += " ";
            str += Integer.toHexString((byte) (b[i] >> 4) & 0x0000000f);
            str += Integer.toHexString((byte) b[i] & 0x0000000f);
        }
        return str;
    }
}
