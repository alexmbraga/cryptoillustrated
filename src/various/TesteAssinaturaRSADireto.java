package various;



import java.security.*;

public class TesteAssinaturaRSADireto {

    public static void main(String args[]) {
        byte[] mensagem = (
                 "Eu, Alexandre Melo Braga, em pleno uso de minhas faculdades"
                +"mentais, declaro para os devidos fins, que todos os alunos do"
                +"curso de criptografia tem nota igual a 10 (dez)."
                ).getBytes();
        try {
            // Gerando um par de chaves RSA de 1024 bits
            KeyPairGenerator gerador = KeyPairGenerator.getInstance("RSA");
            gerador.initialize(1024);
            KeyPair chaves = gerador.generateKeyPair();
            Signature sig = Signature.getInstance("SHA1WithRSA");
            // Inicializa com a chave privada
            sig.initSign(chaves.getPrivate());
            // assina a mensagem e devolve a assinatura
            sig.update(mensagem);
            byte[] assinatura = sig.sign();
            System.out.println("Assinatura "+hexString(assinatura));
            // verifica a assinatura com a chave publica
            sig.initVerify(chaves.getPublic());
            //mensagem[0] = (byte) (mensagem[0] & 0x01);
            sig.update(mensagem);
            if (sig.verify(assinatura)) {
                System.out.println("Assinatura confere.");
            } else {  System.out.println("Assinatura invalida.");}
        } catch (Exception e) { System.out.println(e);}
    }

    private static String hexString(byte[] b) {
        String str = "";
        for (int i = 0; i < b.length; i++) {
            str += Integer.toHexString((byte) (b[i] >> 4) & 0x0000000f);
            str += Integer.toHexString((byte) b[i] & 0x0000000f);
        }
        return str;
    }
}
