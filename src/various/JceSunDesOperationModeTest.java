package various;

import java.security.spec.*;
import javax.crypto.*;
import javax.crypto.spec.*;

class JceSunDesOperationModeTest {

    public static void main(String[] a) {

        try {
            String engine = "DES";
            byte[] theKey = hexToBytes("0123456789ABCDEF");
            byte[] theIVp = null;
            byte[] theMsg = "Alexandre Melo Braga,123".getBytes();
            String algorithm = engine+"/ECB/NoPadding";
            KeySpec ks = new DESKeySpec(theKey);
            SecretKeyFactory kf = SecretKeyFactory.getInstance(engine);
            SecretKey ky = kf.generateSecret(ks);
            Cipher cf = Cipher.getInstance(algorithm);
            cf.init(Cipher.ENCRYPT_MODE, ky);
            byte[] theCph = cf.doFinal(theMsg);
            cf.init(Cipher.DECRYPT_MODE, ky);
            // modificando um byte.
            //theCph[1] = (byte) (theCph[1] & (byte) 0x01);
            byte[] theClear = cf.doFinal(theCph);
            System.out.println("\nTeste 1: " + algorithm);
            System.out.println("Bloco : "+ cf.getBlockSize());
            System.out.println("Key : " + bytesToHex(theKey));
            System.out.println("IV  : " + bytesToHex(theIVp));
            System.out.println("Message    : " + new String(theMsg));
            System.out.println("Msg (bytes): " + bytesToHex(theMsg));
            System.out.println("Cipher     : " + bytesToHex(theCph));
            System.out.println("Clr (bytes): " + bytesToHex(theClear));
            System.out.println("Clear text : " + new String(theClear));

            algorithm = engine+"/CBC/NoPadding";
            theIVp = hexToBytes("1234567890ABCDEF");
            ks = new DESKeySpec(theKey);
            kf = SecretKeyFactory.getInstance(engine);
            ky = kf.generateSecret(ks);
            cf = Cipher.getInstance(algorithm);
            AlgorithmParameterSpec aps = new IvParameterSpec(theIVp);
            cf.init(Cipher.ENCRYPT_MODE, ky, aps);
            theCph = cf.doFinal(theMsg);
            cf.init(Cipher.DECRYPT_MODE, ky, aps);
            // modificando um byte.
            //theCph[1] = (byte) (theCph[1] & (byte) 0x01);
            theClear = cf.doFinal(theCph);
            System.out.println("\nTeste 2: " + algorithm);
            System.out.println("Bloco : "+ cf.getBlockSize());
            System.out.println("Key : " + bytesToHex(theKey));
            System.out.println("IV  : " + bytesToHex(theIVp));
            System.out.println("Message    : " + new String(theMsg));
            System.out.println("Msg (bytes): " + bytesToHex(theMsg));
            System.out.println("Cipher     : " + bytesToHex(theCph));
            System.out.println("Clr (bytes): " + bytesToHex(theClear));
            System.out.println("Clear text : " + new String(theClear));

            algorithm = engine+"/CFB/NoPadding";
            ks = new DESKeySpec(theKey);
            kf = SecretKeyFactory.getInstance(engine);
            ky = kf.generateSecret(ks);
            cf = Cipher.getInstance(algorithm);
            aps = new IvParameterSpec(theIVp);
            cf.init(Cipher.ENCRYPT_MODE, ky, aps);
            theCph = cf.doFinal(theMsg);
            cf.init(Cipher.DECRYPT_MODE, ky, aps);
            // modificando um byte.
            //theCph[1] = (byte) (theCph[1] & (byte) 0x01);
            theClear = cf.doFinal(theCph);
            System.out.println("\nTeste 3: " + algorithm);
            System.out.println("Bloco : "+ cf.getBlockSize());
            System.out.println("Key : " + bytesToHex(theKey));
            System.out.println("IV  : " + bytesToHex(theIVp));
            System.out.println("Message    : " + new String(theMsg));
            System.out.println("Msg (bytes): " + bytesToHex(theMsg));
            System.out.println("Cipher     : " + bytesToHex(theCph));
            System.out.println("Clr (bytes): " + bytesToHex(theClear));
            System.out.println("Clear text : " + new String(theClear));

            algorithm = engine+"/OFB/NoPadding";
            ks = new DESKeySpec(theKey);
            kf = SecretKeyFactory.getInstance(engine);
            ky = kf.generateSecret(ks);
            cf = Cipher.getInstance(algorithm);
            aps = new IvParameterSpec(theIVp);
            cf.init(Cipher.ENCRYPT_MODE, ky, aps);
            theCph = cf.doFinal(theMsg);
            cf.init(Cipher.DECRYPT_MODE, ky, aps);
            // modificando um byte.
            // theCph[1] = (byte) (theCph[1] & (byte) 0x01);
            theClear = cf.doFinal(theCph);
            System.out.println("\nTeste 4: " + algorithm);
            System.out.println("Bloco : "+ cf.getBlockSize());
            System.out.println("Key : " + bytesToHex(theKey));
            System.out.println("IV  : " + bytesToHex(theIVp));
            System.out.println("Message    : " + new String(theMsg));
            System.out.println("Msg (bytes): " + bytesToHex(theMsg));
            System.out.println("Cipher     : " + bytesToHex(theCph));
            System.out.println("Clr (bytes): " + bytesToHex(theClear));
            System.out.println("Clear text : " + new String(theClear));

        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }

    public static byte[] hexToBytes(String str) {
        if (str == null) {
            return null;
        } else if (str.length() < 2) {
            return null;
        } else {
            int len = str.length() / 2;
            byte[] buffer = new byte[len];
            for (int i = 0; i < len; i++) {
                buffer[i] = (byte) Integer.parseInt(
                        str.substring(i * 2, i * 2 + 2), 16);
            }
            return buffer;
        }
    }

    public static String bytesToHex(byte[] data) {
        if (data == null) {
            return null;
        } else {
            int len = data.length;
            String str = "";
            for (int i = 0; i < len; i++) {
                if ((data[i] & 0xFF) < 16) {
                    str = str + "0" + java.lang.Integer.toHexString(data[i] & 0xFF);
                } else {
                    str = str + java.lang.Integer.toHexString(data[i] & 0xFF);
                }
            }
            return str.toUpperCase();
        }
    }
}


