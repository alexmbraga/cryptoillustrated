package various;

import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;

public class TesteAssinaturaRSAPassoAPasso {

    public static void main(String args[]) {
        byte[] mensagem = (
                 "Eu, Alexandre Melo Braga, em pleno gozo de minhas faculdades"
                +"mentais, declaro para os devidos fins, que todos os alunos do"
                +"curso de criptografia tem nota igual a 10 (dez).").getBytes();
        try {
            // Gera o hash da messagem
            MessageDigest md = MessageDigest.getInstance("SHA1");
            md.update(mensagem);
            byte[] hashGerado = md.digest();
            // Gerando um par de chaves RSA de 1024 bits
            KeyPairGenerator gerador = KeyPairGenerator.getInstance("RSA");
            gerador.initialize(1024);
            KeyPair chaves = gerador.generateKeyPair();
            // Cria a implementacao de RSA
            Cipher cifra = Cipher.getInstance("RSA");
            // Criptografando o hash com a chave Privada (para assinatura)
            // inicializa o algoritmo para criptografia
            cifra.init(Cipher.ENCRYPT_MODE, chaves.getPrivate());
            // criptografa o hash
            byte[] hashAssinado = cifra.doFinal(hashGerado);

            // Verificando a assinatura
            // Primeiro, gera o hash a partir da mensagem original
            // estamos apenas usando um outro metodo, para fazer
            // o digest de uma vez so.

            // modificando a mensagem
            //mensagem[0]= (byte)(mensagem[0] & 0x01);
            byte[] hashCalculado = md.digest(mensagem);
            // Descriptografando o hash com a chave Publica
            // (assim, verificamos a assinatura)
            cifra.init(Cipher.DECRYPT_MODE, chaves.getPublic());
            byte[] hashOriginal = cifra.doFinal(hashAssinado);
            // Agora, verificamos se o hash recebido eh igual ao hash calculado
            if (Arrays.equals(hashOriginal, hashCalculado)) {
                System.out.println ("Assinatura confere.");
            } else {
                System.out.println ("Assinatura NÃO confere.");
            }

        } catch  (Exception e) { System.out.println(e); }
    }

    private static String hexString(byte[] b) {
        String str = "";
        for (int i = 0; i < b.length; i++) {
            str += Integer.toHexString((byte) (b[i] >> 4) & 0x0000000f);
            str += Integer.toHexString((byte) b[i] & 0x0000000f);
        }
        return str;
    }

}
