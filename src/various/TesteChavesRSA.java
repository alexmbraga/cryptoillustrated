package various;

import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;

public class TesteChavesRSA {

    public static void main(String args[]) {

        try {
            // Gerando um par de chaves RSA de 1024 bits
            KeyPairGenerator gerador = KeyPairGenerator.getInstance("RSA");
            gerador.initialize(1024);
            KeyPair chaves = gerador.generateKeyPair();

            System.out.println("Algoritmo de cifração "
                        + chaves.getPublic().getAlgorithm());
            System.out.println("\nFormato da Chave pública "
                        + chaves.getPublic().getFormat());
            System.out.println("Chave pública "
                        + chaves.getPublic().toString() );
            System.out.println("Chave pública codificada "
                        + hexString(chaves.getPublic().getEncoded()));

            System.out.println("\nFormato da Chave privada "
                    + chaves.getPrivate().getFormat());
            System.out.println("Chave privada "
                    + chaves.getPrivate().toString());
            System.out.println("Chave privada codificada "
                    + hexString(chaves.getPrivate().getEncoded()));

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private static String hexString(byte[] b) {
        String str = "";
        for (int i = 0; i < b.length; i++) {
            //str += " ";
            str += Integer.toHexString((byte) (b[i] >> 4) & 0x0000000f);
            str += Integer.toHexString((byte) b[i] & 0x0000000f);
        }
        return str;
    }
}
