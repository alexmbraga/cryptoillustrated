package various;

import java.security.*;
import java.util.*;

class JceHashesTest {

    static Object[] hashes = Security.getAlgorithms("MessageDigest").toArray();
    static MessageDigest md[] = new MessageDigest[hashes.length];

    public static void main(String[] a) {
        try {
            System.out.println("Hashs " + Arrays.toString(hashes));
            for (int i = 0; i < md.length; i++) {
                md[i] = MessageDigest.getInstance(hashes[i].toString());
                System.out.println("\nMessage digest object info: ");
                System.out.println(" Algorithm = " + md[i].getAlgorithm());
                System.out.println(" Digest length (bytes) = " + md[i].getDigestLength());
                String input = "";
                md[i].update(input.getBytes());
                byte[] output = md[i].digest();
                System.out.print  ("Hash (\"" + input + "\") = ");
                System.out.println(bytesToHex(output));
                input = "abc";
                md[i].update(input.getBytes());
                output = md[i].digest();
                System.out.print  ("Hash (\"" + input + "\") = ");
                System.out.println(bytesToHex(output));
                input = "abcdefghijklmnopqrstuvwxyz";
                md[i].update(input.getBytes());
                output = md[i].digest();
                System.out.print  ("Hash (\"" + input + "\") = ");
                System.out.println(bytesToHex(output));
            }
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        }
    }

    public static String bytesToHex(byte[] b) {
        char hexDigit[] = {'0', '1', '2', '3', '4', '5', '6', '7',
            '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        StringBuffer buf = new StringBuffer();
        for (int j = 0; j < b.length; j++) {
            buf.append(hexDigit[(b[j] >> 4) & 0x0f]);
            buf.append(hexDigit[b[j] & 0x0f]);
        }
        return buf.toString();
    }
}
