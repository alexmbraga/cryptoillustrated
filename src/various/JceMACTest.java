package various;

/**
 *
 * @author Alexandre
 */
import java.util.Arrays;
import javax.crypto.*;
import java.security.*;
import javax.crypto.spec.*;

public class JceMACTest {

    static Object[] macs = Security.getAlgorithms("Mac").toArray();

    public static void main(String[] args) throws Exception {

        System.out.println("Macs " + Arrays.toString(macs));
        for (int i = 0; i < macs.length; i++) {
            try{
            KeyGenerator kg = KeyGenerator.getInstance(macs[i].toString());
            SecretKey sk = kg.generateKey();
            Mac mac = Mac.getInstance(macs[i].toString());
            mac.init(sk);
            String msg = "Minha terra tem palmeiras, onde canta o sabiá";
            mac.update(msg.getBytes());
            byte[] result = mac.doFinal();
            byte[] key2 = sk.getEncoded();
            System.out.println("\nAlgoritmo: " + mac.getAlgorithm());
            System.out.println("Tamanho : " + mac.getMacLength());
            System.out.println("Chave: " + bytesToHex(key2));
            System.out.println("MSG:   " + msg);
            System.out.println("MAC1:  " + bytesToHex(result));
            SecretKeySpec ks  = new SecretKeySpec(key2,macs[i].toString());
            Mac mac2 = Mac.getInstance(macs[i].toString());
            mac2.init(ks);
            mac2.update(msg.getBytes());
            byte[] result2 = mac2.doFinal();
            System.out.println("MAC2:  " + bytesToHex(result2));
            //result[0] = (byte) (result[0] & 0x01);
            if (Arrays.equals(result,result2)) System.out.println("MAC confere!");
            else System.out.println("MAC Não confere!");
            } catch (Exception e) { 
                System.out.println("\n"+macs[i].toString()+" não disponível\n");
            }
        }
    }

    public static String bytesToHex(byte[] b) {
        char hexDigit[] = {'0', '1', '2', '3', '4', '5', '6', '7',
            '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        StringBuffer buf = new StringBuffer();
        for (int j = 0; j < b.length; j++) {
            buf.append(hexDigit[(b[j] >> 4) & 0x0f]);
            buf.append(hexDigit[b[j] & 0x0f]);
        }
        return buf.toString();
    }

}
