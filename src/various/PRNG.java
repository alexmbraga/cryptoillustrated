package various;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.Random;
import java.security.SecureRandom;

/**
 *
 * @author Alexandre
 */
public class PRNG {

    private static SecureRandom rng = new SecureRandom();

    private static int getRandomInt() {
        int result;
        byte[] b = new byte[4];
        rng.nextBytes(b);
        // converte 4 bytes em um número entre 2 e 2^(32-1)
        result = (int) b[0];
        result = result * 256 + (int) b[1];
        result = result * 256 + (int) b[2];
        result = result * 256 + (int) b[3];
        return result;
    }

    private static int getRandomIntInRange(int n) {
        int d = (int) getRandomInt();
        if (d < 0) {
            d *= -1;
        }
        d = (d % n);
        return d;
    }

    public static void main(String[] args) {
        int range = 10;
        System.out.println("gerando " + range + " números entre 0 e " + range);
        for (int i = 0; i < range; i++) {
            //int r = getRandomInt();
            int r = getRandomIntInRange(range + 1);
            System.out.println(r);
        }
        System.out.println("Algoritmo utilizado: " + rng.getAlgorithm());
        System.out.println("Provedor criptográfico " + rng.getProvider());
        try {
            String prng = "SHA1PRNG";
            String prov = "SUN";
            System.out.println("Teste 1: Dispersão estatística - SecureRandom - SUN");
            SecureRandom sr1 = SecureRandom.getInstance("SHA1PRNG","SUN");
            SecureRandom sr2 = SecureRandom.getInstance("SHA1PRNG","SUN");
            SecureRandom sr3 = SecureRandom.getInstance("SHA1PRNG","SUN");
            for (int i = 0; i < 100; i++) {
                if (i == 0) { System.out.println("i , sr1 , sr2, sr3");}
                System.out.println(i + ", " +
                        sr1.nextInt(10000) + ", " +
                        sr2.nextInt(10000) + ", " +
                        sr3.nextInt(10000));
            }
            System.out.println("Teste 2: Imprevisibilidade - SecureRandom - SUN");
            SecureRandom sr4 = SecureRandom.getInstance("SHA1PRNG","SUN");
            SecureRandom sr5 = SecureRandom.getInstance("SHA1PRNG","SUN");
            byte[] seed = sr4.generateSeed(32);
            sr4.setSeed(seed); // mesma semente
            sr5.setSeed(seed);
            for (int i = 0; i < 100; i++) {
                if (i == 0) {System.out.println("i , sr4 , sr5");}
                System.out.println(i + "," + sr4.nextInt(10000) + ","
                                           + sr5.nextInt(10000));
            }

            prng = "Windows-PRNG";
            prov = "SunMSCAPI";
            System.out.println("Teste 3: Dispersão estatística - SecureRandom - MSCAPI");
            SecureRandom sr6 = SecureRandom.getInstance("Windows-PRNG","SunMSCAPI");
            SecureRandom sr7 = SecureRandom.getInstance("Windows-PRNG","SunMSCAPI");
            SecureRandom sr8 = SecureRandom.getInstance("Windows-PRNG","SunMSCAPI");
            for (int i = 0; i < 100; i++) {
                if (i == 0) {System.out.println("i , sr6 , sr7, sr8");}
                System.out.println(i + ", " +
                        sr6.nextInt(10000) + ", " +
                        sr7.nextInt(10000) + ", " +
                        sr8.nextInt(10000));
            }
            System.out.println("Teste 4: Imprevisibilidade - SecureRandom - MSCAPI");
            SecureRandom sr9 = SecureRandom.getInstance("Windows-PRNG","SunMSCAPI");
            SecureRandom sr0 = SecureRandom.getInstance("Windows-PRNG","SunMSCAPI");
            seed = sr9.generateSeed(32);
            sr9.setSeed(seed); // mesma semente
            sr0.setSeed(seed);
            for (int i = 0; i < 100; i++) {
                if (i == 0) {System.out.println("i , sr9 , sr0");}
                System.out.println(i + "," + sr9.nextInt(10000) + ","
                                           + sr0.nextInt(10000));
            }


            System.out.println("Teste 5: Dispersão estatística - Random");
            Random r1 = new Random(); Random r2 = new Random();
            for (int i = 0; i < 100; i++) {
                if (i == 0) {System.out.println("i , r1 , r2");}
                System.out.println(i + "," + r1.nextInt(10000) + ","
                                           + r2.nextInt(10000));
            }

            System.out.println("Teste 6: Imprevisibilidade - Random");
            Random r3 = new Random(); r3.setSeed(10);
            Random r4 = new Random(); r4.setSeed(10);
            for (int i = 0; i < 100; i++) {
                if (i == 0) {System.out.println("i , r3 , r4");}
                System.out.println(i + "," + r3.nextInt(10000) + ","
                                           + r4.nextInt(10000));
            }

            System.out.println("Teste 7: Dispersão - Math.random");
            for (int i = 0; i < 100; i++) {
                if (i == 0) {System.out.println("i , math.r");}
                System.out.println(i + ", " + (int) (Math.random() * 10000));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static String bytesToHex(byte[] b) {
        char hexDigit[] = {'0', '1', '2', '3', '4', '5', '6', '7',
            '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        StringBuffer buf = new StringBuffer();
        for (int j = 0; j < b.length; j++) {
            buf.append(" ");
            buf.append(hexDigit[(b[j] >> 4) & 0x0f]);
            buf.append(hexDigit[b[j] & 0x0f]);
        }
        return buf.toString();
    }
}
